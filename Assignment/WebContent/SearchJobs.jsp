<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<style>
.navbar {
	margin-bottom: 0;
	background-color: lime;
	z-index: 9999;
	border: 0;
	font-size: 18px !important;
	line-height: 1.42857143 !important;
	letter-spacing: 3px;
	border-radius: 0;
}

.navbar li a, .navbar .navbar-brand {
	color: white !important;
}

.navbar-nav li a:hover, .navbar-nav li.active a {
	color: #f4511e !important;
	background-color: green !important;
}
</style>
	
</head>
<body style="background-color: goldenrod">
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#myPage"><h4>SEARCHJOBS</h4></a>
			</div>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="Loginpage.jsp"><h4>BACK</h4></a></li>
			</ul>
		</div>
	</nav>
	<br>
	<br>
	<br>
	<br>
	<br>
	       
		<form action="ViewJobs.jsp" method="post" class="form-horizontal">
			<div class="form-group">
				<label class="control-label col-sm-4" for="Name">Job Title:</label>
				<div class="col-sm-4">
					<input type="text" name="Jobtitle">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4" for="City">City:</label>
				<div class="col-sm-4">
					<div class="checkbox">
						<select name="Location">
							<option value=" ">Select City</option>
							<option value="chennai">chennai</option>
							<option value="Andhra Pradesh">Andhra Pradesh</option>
							<option value="Bangalore">Bangalore</option>
							<option value="Ananthapur">Ananthapur</option>
						</select>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-4 col-sm-4">
					<input type="submit" value="Search" />

				</div>
		</form>
	</div>

</body>
</html>